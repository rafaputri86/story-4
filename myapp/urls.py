from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('Experience/', views.Experience, name ='Experience'),
    path('Profile/', views.Profile, name ='Profile'),
    path('Contact/', views.Contact, name = 'Contact'),
]
