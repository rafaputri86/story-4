from django.shortcuts import render

def index(request):
	return render(request, 'index.html')

def Profile(request):
    return render(request, 'profile.html')

def Experience(request):
    return render(request, 'experience.html')

def Contact(request):
    return render(request, 'contact.html')

